import React from 'react';
import PropTypes from 'prop-types';
import CropperCanvas from '../CropperCanvas';
import CropperControls from '../CropperControls';

const CropperContainer = (props) => {
  const {
    onSubmit,
    onCancel,
    cropperContainerVisible,
    renderCropperControls,
    renderCropperCanvas,
  } = props;

  const runRenderCropperCanvas = () => {
    if (renderCropperCanvas) {
      return renderCropperCanvas(props);
    }

    return <CropperCanvas {...props} />;
  };

  const runRenderCropperControls = () => {
    if (renderCropperControls) {
      return renderCropperControls(props);
    }

    return (
      <CropperControls
        onSubmit={onSubmit}
        onCancel={onCancel}
      />
    );
  };

  return (
    <div>
      {cropperContainerVisible &&
        <div>
          {runRenderCropperCanvas()}
          {runRenderCropperControls()}
        </div>
      }
    </div>
  );
};

CropperContainer.propTypes = {
  cropperContainerVisible: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  renderCropperControls: PropTypes.func,
  renderCropperCanvas: PropTypes.func,
};

CropperContainer.defaultProps = {
  renderCropperControls: null,
  renderCropperCanvas: null,
};

export default CropperContainer;
