import React from 'react';
import PropTypes from 'prop-types';

const RemoveButton = (props) => {
  const {
    hasAvatar,
    onRemove,
  } = props;

  if (hasAvatar) {
    return (
      <button
        className="btn-remove"
        onClick={onRemove}
      >
        x
      </button>
    );
  }

  return null;
};

RemoveButton.propTypes = {
  hasAvatar: PropTypes.bool,
  onRemove: PropTypes.func.isRequired,
};

RemoveButton.defaultProps = {
  hasAvatar: false,
};

export default RemoveButton;
