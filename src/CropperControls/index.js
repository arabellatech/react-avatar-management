import React from 'react';
import PropTypes from 'prop-types';

const CropperControls = (props) => {
  const {
    onSubmit,
    onCancel,
  } = props;

  return (
    <div className="cropper-controls-container">
      <button
        className="button button--error"
        type="button"
        onClick={onCancel}
      >
        Cancel
      </button>

      <button
        className="button button--success"
        type="button"
        onClick={onSubmit}
      >
        Save
      </button>
    </div>
  );
};

CropperControls.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
};

export default CropperControls;
