import React from 'react';
import PropTypes from 'prop-types';

const ThumbnailSpinner = (props) => {
  const {
    isLoading,
  } = props;

  if (isLoading) {
    return (
      <div className="thumbnail-spinner-container">
        <div className="spinner"></div>
      </div>
    );
  }

  return null;
};

ThumbnailSpinner.propTypes = {
  isLoading: PropTypes.bool,
};

ThumbnailSpinner.defaultProps = {
  isLoading: false,
};

export default ThumbnailSpinner;
