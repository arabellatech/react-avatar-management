import React from 'react';
import PropTypes from 'prop-types';
import RemoveButton from '../RemoveButton';
import ThumbnailPreview from '../ThumbnailPreview';
import ThumbnailSpinner from '../ThumbnailSpinner';
import Thumbnail from '../Thumbnail';

const ThumbnailContainer = (props) => {
  const {
    renderThumbnail,
    renderRemoveButton,
    renderSpinner,
    isLoading,
    isSubmitting,
    isRemoving,
    isDisabled,
  } = props;

  const runRenderThumbnail = () => {
    if (renderThumbnail) {
      return renderThumbnail(props);
    }

    return (
      <Thumbnail {...props} >
        <ThumbnailPreview {...props} />
      </Thumbnail>
    );
  };

  const runRenderRemoveButton = () => {
    if (renderRemoveButton) {
      return renderRemoveButton(props);
    }

    if (isDisabled) {
      return null;
    }

    return <RemoveButton {...props} />;
  };

  const runRenderSpinner = () => {
    if (renderSpinner) {
      return renderSpinner(props);
    }

    const isProcessing = isLoading || isSubmitting || isRemoving;

    return (
      <ThumbnailSpinner
        {...props}
        isLoading={isProcessing}
      />
    );
  };

  return (
    <div className="thumbnail-container">
      {runRenderThumbnail()}
      {runRenderRemoveButton()}
      {runRenderSpinner()}
    </div>
  );
};

ThumbnailContainer.propTypes = {
  renderThumbnail: PropTypes.func,
  renderRemoveButton: PropTypes.func,
  renderSpinner: PropTypes.func,
  isLoading: PropTypes.bool,
  isSubmitting: PropTypes.bool,
  isRemoving: PropTypes.bool,
  isDisabled: PropTypes.bool,
};

ThumbnailContainer.defaultProps = {
  renderThumbnail: null,
  renderRemoveButton: null,
  renderSpinner: null,
  isLoading: false,
  isSubmitting: false,
  isRemoving: false,
  isDisabled: false,
};

export default ThumbnailContainer;
