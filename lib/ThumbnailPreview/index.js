'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ThumbnailPreview = function ThumbnailPreview(props) {
  var avatar = props.avatar,
      temporaryAvatar = props.temporaryAvatar,
      placeholderText = props.placeholderText,
      disabledText = props.disabledText,
      isDisabled = props.isDisabled;


  if (avatar || temporaryAvatar) {
    return _react2.default.createElement(
      'div',
      { className: 'thumbnail-preview' },
      _react2.default.createElement(
        'div',
        { className: 'thumbnail-preview-caption' },
        isDisabled ? disabledText : placeholderText
      ),
      _react2.default.createElement(
        'div',
        { className: 'thumbnail-preview-image-container' },
        _react2.default.createElement('img', {
          className: 'thumbnail-preview-image',
          alt: 'avatar',
          src: avatar || temporaryAvatar
        })
      )
    );
  }

  return _react2.default.createElement(
    'div',
    { className: 'thumbnail-preview' },
    _react2.default.createElement(
      'p',
      { className: 'thumbnail-preview-placeholder' },
      isDisabled ? disabledText : placeholderText
    )
  );
};

ThumbnailPreview.propTypes = {
  avatar: _propTypes2.default.string,
  temporaryAvatar: _propTypes2.default.string,
  placeholderText: _propTypes2.default.string.isRequired,
  disabledText: _propTypes2.default.string.isRequired,
  isDisabled: _propTypes2.default.bool
};

ThumbnailPreview.defaultProps = {
  avatar: undefined,
  temporaryAvatar: undefined,
  isDisabled: false
};

exports.default = ThumbnailPreview;