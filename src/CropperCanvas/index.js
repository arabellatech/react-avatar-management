import React from 'react';
import PropTypes from 'prop-types';
import Cropper from 'react-cropper';

const CropperCanvas = (props) => {
  const {
    cropperOptions,
  } = props;

  return <Cropper {...cropperOptions} />;
};

CropperCanvas.propTypes = {
  cropperOptions: PropTypes.object.isRequired,
};

export default CropperCanvas;
