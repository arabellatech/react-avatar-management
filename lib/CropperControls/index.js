'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CropperControls = function CropperControls(props) {
  var onSubmit = props.onSubmit,
      onCancel = props.onCancel;


  return _react2.default.createElement(
    'div',
    { className: 'cropper-controls-container' },
    _react2.default.createElement(
      'button',
      {
        className: 'button button--error',
        type: 'button',
        onClick: onCancel
      },
      'Cancel'
    ),
    _react2.default.createElement(
      'button',
      {
        className: 'button button--success',
        type: 'button',
        onClick: onSubmit
      },
      'Save'
    )
  );
};

CropperControls.propTypes = {
  onSubmit: _propTypes2.default.func.isRequired,
  onCancel: _propTypes2.default.func.isRequired
};

exports.default = CropperControls;