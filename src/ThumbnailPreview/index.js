import React from 'react';
import PropTypes from 'prop-types';

const ThumbnailPreview = (props) => {
  const {
    avatar,
    temporaryAvatar,
    placeholderText,
    disabledText,
    isDisabled,
  } = props;

  if (avatar || temporaryAvatar) {
    return (
      <div className="thumbnail-preview">
        <div className="thumbnail-preview-caption">{isDisabled ? disabledText : placeholderText}</div>
        <div className="thumbnail-preview-image-container">
          <img
            className="thumbnail-preview-image"
            alt="avatar"
            src={avatar || temporaryAvatar}
          />
        </div>
      </div>
    );
  }

  return (
    <div className="thumbnail-preview">
      <p className="thumbnail-preview-placeholder">
        {isDisabled ? disabledText : placeholderText}
      </p>
    </div>
  );
};

ThumbnailPreview.propTypes = {
  avatar: PropTypes.string,
  temporaryAvatar: PropTypes.string,
  placeholderText: PropTypes.string.isRequired,
  disabledText: PropTypes.string.isRequired,
  isDisabled: PropTypes.bool,
};

ThumbnailPreview.defaultProps = {
  avatar: undefined,
  temporaryAvatar: undefined,
  isDisabled: false,
};

export default ThumbnailPreview;
