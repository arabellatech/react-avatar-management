'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _RemoveButton = require('../RemoveButton');

var _RemoveButton2 = _interopRequireDefault(_RemoveButton);

var _ThumbnailPreview = require('../ThumbnailPreview');

var _ThumbnailPreview2 = _interopRequireDefault(_ThumbnailPreview);

var _ThumbnailSpinner = require('../ThumbnailSpinner');

var _ThumbnailSpinner2 = _interopRequireDefault(_ThumbnailSpinner);

var _Thumbnail = require('../Thumbnail');

var _Thumbnail2 = _interopRequireDefault(_Thumbnail);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ThumbnailContainer = function ThumbnailContainer(props) {
  var renderThumbnail = props.renderThumbnail,
      renderRemoveButton = props.renderRemoveButton,
      renderSpinner = props.renderSpinner,
      isLoading = props.isLoading,
      isSubmitting = props.isSubmitting,
      isRemoving = props.isRemoving,
      isDisabled = props.isDisabled;


  var runRenderThumbnail = function runRenderThumbnail() {
    if (renderThumbnail) {
      return renderThumbnail(props);
    }

    return _react2.default.createElement(
      _Thumbnail2.default,
      props,
      _react2.default.createElement(_ThumbnailPreview2.default, props)
    );
  };

  var runRenderRemoveButton = function runRenderRemoveButton() {
    if (renderRemoveButton) {
      return renderRemoveButton(props);
    }

    if (isDisabled) {
      return null;
    }

    return _react2.default.createElement(_RemoveButton2.default, props);
  };

  var runRenderSpinner = function runRenderSpinner() {
    if (renderSpinner) {
      return renderSpinner(props);
    }

    var isProcessing = isLoading || isSubmitting || isRemoving;

    return _react2.default.createElement(_ThumbnailSpinner2.default, _extends({}, props, {
      isLoading: isProcessing
    }));
  };

  return _react2.default.createElement(
    'div',
    { className: 'thumbnail-container' },
    runRenderThumbnail(),
    runRenderRemoveButton(),
    runRenderSpinner()
  );
};

ThumbnailContainer.propTypes = {
  renderThumbnail: _propTypes2.default.func,
  renderRemoveButton: _propTypes2.default.func,
  renderSpinner: _propTypes2.default.func,
  isLoading: _propTypes2.default.bool,
  isSubmitting: _propTypes2.default.bool,
  isRemoving: _propTypes2.default.bool,
  isDisabled: _propTypes2.default.bool
};

ThumbnailContainer.defaultProps = {
  renderThumbnail: null,
  renderRemoveButton: null,
  renderSpinner: null,
  isLoading: false,
  isSubmitting: false,
  isRemoving: false,
  isDisabled: false
};

exports.default = ThumbnailContainer;