import React from 'react';
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone';

const Thumbnail = (props) => {
  const {
    thumbnailOptions,
    children,
  } = props;

  return (
    <Dropzone {...thumbnailOptions} >
      {children}
    </Dropzone>
  );
};

Thumbnail.propTypes = {
  thumbnailOptions: PropTypes.object.isRequired,
  children: PropTypes.node.isRequired,
};

export default Thumbnail;
