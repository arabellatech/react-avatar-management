'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _CropperCanvas = require('../CropperCanvas');

var _CropperCanvas2 = _interopRequireDefault(_CropperCanvas);

var _CropperControls = require('../CropperControls');

var _CropperControls2 = _interopRequireDefault(_CropperControls);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CropperContainer = function CropperContainer(props) {
  var onSubmit = props.onSubmit,
      onCancel = props.onCancel,
      cropperContainerVisible = props.cropperContainerVisible,
      renderCropperControls = props.renderCropperControls,
      renderCropperCanvas = props.renderCropperCanvas;


  var runRenderCropperCanvas = function runRenderCropperCanvas() {
    if (renderCropperCanvas) {
      return renderCropperCanvas(props);
    }

    return _react2.default.createElement(_CropperCanvas2.default, props);
  };

  var runRenderCropperControls = function runRenderCropperControls() {
    if (renderCropperControls) {
      return renderCropperControls(props);
    }

    return _react2.default.createElement(_CropperControls2.default, {
      onSubmit: onSubmit,
      onCancel: onCancel
    });
  };

  return _react2.default.createElement(
    'div',
    null,
    cropperContainerVisible && _react2.default.createElement(
      'div',
      null,
      runRenderCropperCanvas(),
      runRenderCropperControls()
    )
  );
};

CropperContainer.propTypes = {
  cropperContainerVisible: _propTypes2.default.bool.isRequired,
  onSubmit: _propTypes2.default.func.isRequired,
  onCancel: _propTypes2.default.func.isRequired,
  renderCropperControls: _propTypes2.default.func,
  renderCropperCanvas: _propTypes2.default.func
};

CropperContainer.defaultProps = {
  renderCropperControls: null,
  renderCropperCanvas: null
};

exports.default = CropperContainer;