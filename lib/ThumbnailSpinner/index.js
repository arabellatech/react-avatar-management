'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ThumbnailSpinner = function ThumbnailSpinner(props) {
  var isLoading = props.isLoading;


  if (isLoading) {
    return _react2.default.createElement(
      'div',
      { className: 'thumbnail-spinner-container' },
      _react2.default.createElement('div', { className: 'spinner' })
    );
  }

  return null;
};

ThumbnailSpinner.propTypes = {
  isLoading: _propTypes2.default.bool
};

ThumbnailSpinner.defaultProps = {
  isLoading: false
};

exports.default = ThumbnailSpinner;