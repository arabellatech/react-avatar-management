'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactCropper = require('react-cropper');

var _reactCropper2 = _interopRequireDefault(_reactCropper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CropperCanvas = function CropperCanvas(props) {
  var cropperOptions = props.cropperOptions;


  return _react2.default.createElement(_reactCropper2.default, cropperOptions);
};

CropperCanvas.propTypes = {
  cropperOptions: _propTypes2.default.object.isRequired
};

exports.default = CropperCanvas;