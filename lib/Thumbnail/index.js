'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactDropzone = require('react-dropzone');

var _reactDropzone2 = _interopRequireDefault(_reactDropzone);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Thumbnail = function Thumbnail(props) {
  var thumbnailOptions = props.thumbnailOptions,
      children = props.children;


  return _react2.default.createElement(
    _reactDropzone2.default,
    thumbnailOptions,
    children
  );
};

Thumbnail.propTypes = {
  thumbnailOptions: _propTypes2.default.object.isRequired,
  children: _propTypes2.default.node.isRequired
};

Thumbnail.defaultProps = {};

exports.default = Thumbnail;