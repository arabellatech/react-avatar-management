import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import _bindAll from 'lodash/bindAll';
import 'cropperjs/dist/cropper.css';

import CropperContainer from '../CropperContainer';
import ThumbnailContainer from '../ThumbnailContainer';

export default class AvatarManagement extends PureComponent {
  static defaultProps = {
    avatar: undefined,
    outputType: 'base64',
    placeholderText: 'Drop your file here or click to pick it manually',
    disabledText: 'Avatar edition is disabled',

    // Callbacks
    onAvatarPick: () => undefined,
    onAvatarRemove: () => undefined,

    // States
    isLoading: false,
    isSubmitting: false,
    isRemoving: false,
    isDisabled: false,

    // Custom renderers
    renderCropperContainer: null,
    renderCropperCanvas: null,
    renderCropperControls: null,

    renderThumbnailContainer: null,
    renderThumbnail: null,
    renderThumbnailPreview: null,
    renderRemoveButton: null,

    renderSpinner: null,

    // Third-party components options
    cropperOptions: {},
    thumbnailOptions: {},
  };

  static propTypes = {
    avatar: PropTypes.string,
    outputType: PropTypes.string,
    placeholderText: PropTypes.string,
    disabledText: PropTypes.string,

    onAvatarPick: PropTypes.func,
    onAvatarRemove: PropTypes.func,

    isSubmitting: PropTypes.bool,
    isRemoving: PropTypes.bool,
    isDisabled: PropTypes.bool,

    renderCropperContainer: PropTypes.func,
    renderCropperCanvas: PropTypes.func,
    renderCropperControls: PropTypes.func,

    renderThumbnailContainer: PropTypes.func,
    renderThumbnail: PropTypes.func,
    renderThumbnailPreview: PropTypes.func,
    renderRemoveButton: PropTypes.func,

    renderSpinner: PropTypes.func,

    cropperOptions: PropTypes.object,
    thumbnailOptions: PropTypes.object,
  }

  constructor(props) {
    super(props);

    this.state = {
      avatarTemp: undefined,
      isSubmitting: this.props.isSubmitting,
      isRemoving: this.props.isRemoving,
      cropperContainerVisible: false,
    };

    this.dropzoneRef = undefined;
    this.cropperRef = undefined;

    _bindAll(this, [
      'handleDrop',
      'handleSubmit',
      'handleCancel',
      'composeCropperContainerProps',
      'composeThumbnailContainerProps',
      'renderCropperContainer',
      'renderThumbnailContainer',
    ]);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.avatar) {
      this.setState({
        avatarTemp: undefined,
      });
    }
  }

  handleDrop(accepted) {
    this.setState({
      avatarTemp: accepted[0].preview,
      type: accepted[0].type,
      cropperContainerVisible: true,
    });
  }

  handleSubmit() {
    const {
      outputType,
      onAvatarPick,
    } = this.props;

    const canvas = this.cropperRef.getCroppedCanvas();
    const canvasOutput = canvas.toDataURL();
    const canvasPureOutput = canvasOutput.split(',')[1];

    this.setState({
      avatarTemp: canvasOutput,
      cropperContainerVisible: false,
    });

    if (outputType !== 'blob' && outputType !== 'base64') {
      throw new Error('Unknown output type: ', outputType);
    }

    if (outputType === 'blob') {
      canvas.toBlob((blob) => {
        onAvatarPick(blob);
      });
    }

    if (outputType === 'base64') {
      onAvatarPick(canvasPureOutput);
    }
  }

  handleCancel() {
    this.setState({
      avatarTemp: undefined,
      cropperContainerVisible: false,
    });
  }

  composeThumbnailContainerProps() {
    const {
      avatar,
      isDisabled,
      thumbnailOptions,
      onAvatarRemove,
    } = this.props;

    return {
      ...this.props,
      onRemove: onAvatarRemove,
      hasAvatar: !!avatar,
      thumbnailOptions: {
        ref: (node) => { this.dropzoneRef = node; },
        accept: 'image/jpeg, image/png, image/gif',
        multiple: false,
        disableClick: isDisabled,
        onDrop: isDisabled ? undefined : this.handleDrop,
        className: 'thumbnail',
        ...thumbnailOptions,
      },
    };
  }

  composeCropperContainerProps() {
    const {
      cropperContainerVisible,
      avatarTemp,
    } = this.state;

    const {
      avatar,
      cropperOptions,
    } = this.props;

    return {
      ...this.props,
      onSubmit: this.handleSubmit,
      onCancel: this.handleCancel,
      cropperContainerVisible,
      cropperOptions: {
        ref: (node) => { this.cropperRef = node; },
        src: avatarTemp || avatar,
        aspectRatio: 1 / 1,
        guides: true,
        style: {
          width: '100%',
          height: '400px',
        },
        ...cropperOptions,
      },
    };
  }

  renderThumbnailContainer() {
    const { renderThumbnailContainer } = this.props;
    const thumbnailContainerProps = this.composeThumbnailContainerProps();

    if (renderThumbnailContainer) {
      return renderThumbnailContainer(thumbnailContainerProps);
    }

    return <ThumbnailContainer {...thumbnailContainerProps} />;
  }

  renderCropperContainer() {
    const { renderCropperContainer } = this.props;
    const cropperContainerProps = this.composeCropperContainerProps();

    if (renderCropperContainer) {
      return renderCropperContainer(cropperContainerProps);
    }

    return <CropperContainer {...cropperContainerProps} />;
  }

  render() {
    return (
      <div className="avatar-management-container">
        {this.renderThumbnailContainer()}
        {this.renderCropperContainer()}
      </div>
    );
  }
}
