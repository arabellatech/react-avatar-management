'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _bindAll2 = require('lodash/bindAll');

var _bindAll3 = _interopRequireDefault(_bindAll2);

require('cropperjs/dist/cropper.css');

var _CropperContainer = require('../CropperContainer');

var _CropperContainer2 = _interopRequireDefault(_CropperContainer);

var _ThumbnailContainer = require('../ThumbnailContainer');

var _ThumbnailContainer2 = _interopRequireDefault(_ThumbnailContainer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AvatarManagement = (_temp = _class = function (_PureComponent) {
  _inherits(AvatarManagement, _PureComponent);

  function AvatarManagement(props) {
    _classCallCheck(this, AvatarManagement);

    var _this = _possibleConstructorReturn(this, (AvatarManagement.__proto__ || Object.getPrototypeOf(AvatarManagement)).call(this, props));

    _this.state = {
      avatarTemp: undefined,
      isSubmitting: _this.props.isSubmitting,
      isRemoving: _this.props.isRemoving,
      cropperContainerVisible: false
    };

    _this.dropzoneRef = undefined;
    _this.cropperRef = undefined;

    (0, _bindAll3.default)(_this, ['handleDrop', 'handleSubmit', 'handleCancel', 'composeCropperContainerProps', 'composeThumbnailContainerProps', 'renderCropperContainer', 'renderThumbnailContainer']);
    return _this;
  }

  _createClass(AvatarManagement, [{
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      if (nextProps.avatar) {
        this.setState({
          avatarTemp: undefined
        });
      }
    }
  }, {
    key: 'handleDrop',
    value: function handleDrop(accepted) {
      this.setState({
        avatarTemp: accepted[0].preview,
        type: accepted[0].type,
        cropperContainerVisible: true
      });
    }
  }, {
    key: 'handleSubmit',
    value: function handleSubmit() {
      var _props = this.props,
          outputType = _props.outputType,
          onAvatarPick = _props.onAvatarPick;


      var canvas = this.cropperRef.getCroppedCanvas();
      var canvasOutput = canvas.toDataURL();
      var canvasPureOutput = canvasOutput.split(',')[1];

      this.setState({
        avatarTemp: canvasOutput,
        cropperContainerVisible: false
      });

      if (outputType !== 'blob' && outputType !== 'base64') {
        throw new Error('Unknown output type: ', outputType);
      }

      if (outputType === 'blob') {
        canvas.toBlob(function (blob) {
          onAvatarPick(blob);
        });
      }

      if (outputType === 'base64') {
        onAvatarPick(canvasPureOutput);
      }
    }
  }, {
    key: 'handleCancel',
    value: function handleCancel() {
      this.setState({
        avatarTemp: undefined,
        cropperContainerVisible: false
      });
    }
  }, {
    key: 'composeThumbnailContainerProps',
    value: function composeThumbnailContainerProps() {
      var _this2 = this;

      var _props2 = this.props,
          avatar = _props2.avatar,
          isDisabled = _props2.isDisabled,
          thumbnailOptions = _props2.thumbnailOptions,
          onAvatarRemove = _props2.onAvatarRemove;


      return _extends({}, this.props, {
        onRemove: onAvatarRemove,
        hasAvatar: !!avatar,
        thumbnailOptions: _extends({
          ref: function ref(node) {
            _this2.dropzoneRef = node;
          },
          accept: 'image/jpeg, image/png, image/gif',
          multiple: false,
          disableClick: isDisabled,
          onDrop: isDisabled ? undefined : this.handleDrop,
          className: 'thumbnail'
        }, thumbnailOptions)
      });
    }
  }, {
    key: 'composeCropperContainerProps',
    value: function composeCropperContainerProps() {
      var _this3 = this;

      var _state = this.state,
          cropperContainerVisible = _state.cropperContainerVisible,
          avatarTemp = _state.avatarTemp;
      var _props3 = this.props,
          avatar = _props3.avatar,
          cropperOptions = _props3.cropperOptions;


      return _extends({}, this.props, {
        onSubmit: this.handleSubmit,
        onCancel: this.handleCancel,
        cropperContainerVisible: cropperContainerVisible,
        cropperOptions: _extends({
          ref: function ref(node) {
            _this3.cropperRef = node;
          },
          src: avatarTemp || avatar,
          aspectRatio: 1 / 1,
          guides: true,
          style: {
            width: '100%',
            height: '400px'
          }
        }, cropperOptions)
      });
    }
  }, {
    key: 'renderThumbnailContainer',
    value: function renderThumbnailContainer() {
      var renderThumbnailContainer = this.props.renderThumbnailContainer;

      var thumbnailContainerProps = this.composeThumbnailContainerProps();

      if (renderThumbnailContainer) {
        return renderThumbnailContainer(thumbnailContainerProps);
      }

      return _react2.default.createElement(_ThumbnailContainer2.default, thumbnailContainerProps);
    }
  }, {
    key: 'renderCropperContainer',
    value: function renderCropperContainer() {
      var renderCropperContainer = this.props.renderCropperContainer;

      var cropperContainerProps = this.composeCropperContainerProps();

      if (renderCropperContainer) {
        return renderCropperContainer(cropperContainerProps);
      }

      return _react2.default.createElement(_CropperContainer2.default, cropperContainerProps);
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: 'avatar-management-container' },
        this.renderThumbnailContainer(),
        this.renderCropperContainer()
      );
    }
  }]);

  return AvatarManagement;
}(_react.PureComponent), _class.defaultProps = {
  avatar: undefined,
  outputType: 'base64',
  placeholderText: 'Drop your file here or click to pick it manually',
  disabledText: 'Avatar edition is disabled',

  // Callbacks
  onAvatarPick: function onAvatarPick() {
    return undefined;
  },
  onAvatarRemove: function onAvatarRemove() {
    return undefined;
  },

  // States
  isLoading: false,
  isSubmitting: false,
  isRemoving: false,
  isDisabled: false,

  // Custom renderers
  renderCropperContainer: null,
  renderCropperCanvas: null,
  renderCropperControls: null,

  renderThumbnailContainer: null,
  renderThumbnail: null,
  renderThumbnailPreview: null,
  renderRemoveButton: null,

  renderSpinner: null,

  // Third-party components options
  cropperOptions: {},
  thumbnailOptions: {}
}, _class.propTypes = {
  avatar: _propTypes2.default.string,
  outputType: _propTypes2.default.string,
  placeholderText: _propTypes2.default.string,
  disabledText: _propTypes2.default.string,

  onAvatarPick: _propTypes2.default.func,
  onAvatarRemove: _propTypes2.default.func,

  isSubmitting: _propTypes2.default.bool,
  isRemoving: _propTypes2.default.bool,
  isDisabled: _propTypes2.default.bool,

  renderCropperContainer: _propTypes2.default.func,
  renderCropperCanvas: _propTypes2.default.func,
  renderCropperControls: _propTypes2.default.func,

  renderThumbnailContainer: _propTypes2.default.func,
  renderThumbnail: _propTypes2.default.func,
  renderThumbnailPreview: _propTypes2.default.func,
  renderRemoveButton: _propTypes2.default.func,

  renderSpinner: _propTypes2.default.func,

  cropperOptions: _propTypes2.default.object,
  thumbnailOptions: _propTypes2.default.object
}, _temp);
exports.default = AvatarManagement;